using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.IO;
using UnityEditor.iOS.Xcode;
using UnityEditor.Callbacks;

public class ProjectBuilder {

	static string[] GetScenes() {
		string[] scenes = (from scene in EditorBuildSettings.scenes
						   where scene.enabled
						   select scene.path).ToArray();
		if (scenes.Length == 0) {
			throw new System.Exception("No default scenes found in project settings");
		}
		return scenes;
	}

	static void BuildIos() {
		string[] scenes = GetScenes();
		//EditorAssetManager.EasyBuild();
		BuildPipeline.BuildPlayer(scenes, "outcome/xcodeproject", BuildTarget.iOS, BuildOptions.None);
	}

	static void BuildAndroid() {
		string[] scenes = GetScenes();
		//EditorAssetManager.EasyBuild();
		PlayerSettings.iOS.targetOSVersion = iOSTargetOSVersion.iOS_9_0;
		BuildPipeline.BuildPlayer(scenes, "outcome/android.apk", BuildTarget.Android, BuildOptions.None);
	}

	[PostProcessBuildAttribute(1)]
	static void OnPostProcessBuild(BuildTarget buildTarget, string path)
	{
		string projPath = "outcome/xcodeproject" + "/Unity-iPhone.xcodeproj/project.pbxproj";

		PBXProject proj = new PBXProject();
		proj.ReadFromString(File.ReadAllText(projPath));

		string target = proj.TargetGuidByName("Unity-iPhone");

		// 色々設定更新(実際はJenkinsのオプション引数で各種情報を取得)
		proj.SetBuildProperty(target, "DEVELOPMENT_TEAM", "9L87EU5V8J"); // チーム名はADCで確認できるPrefix値を設定する
		//proj.SetBuildProperty(target, "CODE_SIGN_IDENTITY", "iPhone Distributiron");
		//proj.SetBuildProperty(target, "PROVISIONING_PROFILE_SPECIFIER", "XC Ad Hoc: com.StudioGM.*"); // XCode8からProvisioning名で指定できる

		proj.WriteToFile(projPath);
	}

}